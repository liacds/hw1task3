import java.util.Scanner;
import java.text.DateFormatSymbols;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter year");
        int year = scanner.nextInt();
        System.out.println("Please, enter month (in number)");
        int monthNumber = scanner.nextInt();
        String month= new DateFormatSymbols().getMonths()[monthNumber-1];

        boolean leap = false;

        if(year % 4 == 0)
        {
            if( year % 100 == 0)
            {
                // year is divisible by 400, hence the year is a leap year
                if ( year % 400 == 0)
                    leap = true;
                else
                    leap = false;
            }
            else
                leap = true;
        }
        else
            leap = false;

        System.out.print(year +" "+month +" - ");
        if(leap){
            System.out.print("leap year");
        }
        else{
            System.out.print("not leap year");
        }
    }
}
